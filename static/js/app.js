"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    class: '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts.class = opts.class ? " ".concat(opts.class) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts.class, "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  function documentAddClassOverflowHidden() {
    if (/iPod|iPad|iPhone/i.test(navigator.userAgent)) {
      $('body').addClass('overflow-hidden');
    } else {
      $('body').removeClass('overflow-hidden');
      $(document.documentElement).addClass('overflow-hidden');
    }
  }

  function documentRemoveClassOverflowHidden() {
    $('body').removeClass('overflow-hidden');
    $(document.documentElement).removeClass('overflow-hidden');
  }

  var headerResizeFlag = 1;

  function headerResize() {
    if (window.matchMedia('(max-width:1024px)').matches && headerResizeFlag == 1) {
      if ($('.app-header--type-2').length) {
        $('.app-header__content-bot .app-header__container').appendTo('.mobile-panel__content');
        $('.app-header__search').appendTo('.mobile-panel__content');
      } else {
        $('.app-header__content-bot').appendTo('.mobile-panel__content');
      }

      $('.app-header__menu').appendTo('.mobile-panel__content');
      headerResizeFlag = 2;
    } else if (window.matchMedia('(min-width:1025px)').matches && headerResizeFlag == 2) {
      if ($('.app-header--type-2').length) {
        $('.mobile-panel__content .app-header__container').appendTo('.app-header__content-bot');
        $('.app-header__menu').prependTo('.app-header__content-top .app-header__container');
        $('.app-header__search').appendTo('.app-header__content-top .app-header__container-bot');
      } else {
        $('.app-header__menu').appendTo('.app-header__content-top .app-header__container');
        $('.app-header__content-bot').appendTo('.app-header__content');
      }

      headerResizeFlag = 1;
    }
  }

  $('.app-header__burger').on('click tap', function () {
    $('.mobile-panel').addClass('opened');
    $('.app__overlay').addClass('opened');
    documentAddClassOverflowHidden();
    $('.mobile-panel').css({
      'height': $(window).height() + 60
    });
  });
  $(document).on('click tap', function (event) {
    if ($(event.target).closest('.app-header__burger, .mobile-panel__content').length) return;
    $('.mobile-panel').removeClass('opened');
    $('.app__overlay').removeClass('opened');
    documentRemoveClassOverflowHidden();
  });
  var bCarousel = $('.b-carousel__items'),
      items = $('.b-carousel__item'),
      activeItem = bCarousel.find('.active-item'),
      rotateDeg = 90; // bCarousel.css({'transform' : 'rotate('+ 90 +'deg)'});

  function carouselResize() {
    if ($(window).width() > 1800) {
      $('.b-carousel__item.active-item').css({
        "transform": "rotate(-90deg) scale(1.44)"
      });
      carouselRefreshInterval();
      carouselRotateInterval();
    } else {
      carouselRefreshInterval();
      $('.b-carousel__item').removeAttr('style');
      $('.b-carousel__items').removeAttr('style');
    }
  }

  function rotateCarouselBack() {
    rotateDeg -= 45;
    items.css({
      'transform': 'rotate(' + -rotateDeg + 'deg)'
    });
    bCarousel.css({
      'transform': 'rotate(' + rotateDeg + 'deg)'
    });
    setTimeout(function () {
      if ($('.b-carousel__item.active-item').next().length == 0) {
        items.removeClass('active-item').css({
          'transform': 'rotate(' + -rotateDeg + 'deg)'
        }).first().addClass('active-item').css({
          'transform': 'rotate(' + -rotateDeg + 'deg) scale(1.44)'
        });
      } else {
        $('.b-carousel__item.active-item').next().addClass('active-item').css({
          'transform': 'rotate(' + -rotateDeg + 'deg) scale(1.44)'
        }).prev().removeClass('active-item');
      }

      changeBlock();
    }, 100);
  }

  function rotateCarouselForward() {
    rotateDeg += 45;
    items.css({
      'transform': 'rotate(' + -rotateDeg + 'deg)'
    });
    bCarousel.css({
      'transform': 'rotate(' + rotateDeg + 'deg)'
    });
    setTimeout(function () {
      if ($('.b-carousel__item.active-item').prev().length == 0) {
        items.removeClass('active-item').css({
          'transform': 'rotate(' + -rotateDeg + 'deg)'
        }).last().addClass('active-item').css({
          'transform': 'rotate(' + -rotateDeg + 'deg) scale(1.44)'
        });
      } else {
        $('.b-carousel__item.active-item').prev().addClass('active-item').css({
          'transform': 'rotate(' + -rotateDeg + 'deg) scale(1.44)'
        }).next().removeClass('active-item');
      }

      changeBlock();
    }, 100);
  }

  $('.b-carousel__arrow').on('click', function () {
    var $this = $(this);
    carouselRefreshInterval();
    carouselRotateInterval();

    if ($this.hasClass('prev')) {
      rotateCarouselBack();
    } else {
      rotateCarouselForward();
    }
  });
  var refresh;

  function carouselRotateInterval() {
    refresh = setInterval(function () {
      rotateCarouselForward();
    }, 5000);
  }

  function carouselRefreshInterval() {
    clearInterval(refresh);
  }

  function changeBlock() {
    var dataTabIndex = $('.b-carousel').find('.b-carousel__item.active-item').data('tab-index');
    $('.b-info .b-info__content').hide().removeClass('active-item').filter('[data-tab-index=' + dataTabIndex + ']').fadeIn().addClass('active-item').css({
      'display': $(window).width() > 768 ? 'flex' : 'block'
    });
  }

  $('.b-carousel__item').on('click', function () {
    if ($(window).width() < 1800) {
      $('.b-carousel__item').removeClass('active-item');
      $(this).addClass('active-item');
      changeBlock();
    }
  });

  if ($(window).width() > 1800) {
    $('.b-info').on({
      mouseenter: function mouseenter() {
        carouselRefreshInterval();
      },
      mouseleave: function mouseleave() {
        carouselRotateInterval();
      }
    });
  }

  $('.has-sublevel > a').append('<span class="arrow toggle-menu"></span>');
  $(document).on('click', '.toggle-menu', function (event) {
    event.preventDefault();
    $(this).closest('li').toggleClass('opened');
  });
  var feedbackPopup = $('[data-remodal-id=feddbackModal]').remodal();
  $('._js-feedback').on('click', function (event) {
    event.preventDefault();
    feedbackPopup.open();
  });
  var authorization = $('[data-remodal-id=authorizationModal]').remodal();
  $('._js-authorization').on('click', function (event) {
    event.preventDefault();
    authorization.open();
  });
  $('.remodal-close').append('<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.97 13.97"><title>close</title><path d="M19,6.42,13.41,12,19,17.58,17.58,19,12,13.41,6.42,19,5,17.58,10.59,12,5,6.42,6.42,5,12,10.59,17.58,5Z" transform="translate(-5.02 -5.02)"/></svg>');

  function showIcons() {
    setTimeout(function () {
      $('.b-about__item').first().fadeTo(400, 1, function () {
        $(this).next().fadeTo(400, 1, function () {
          $(this).next().fadeTo(400, 1, function () {
            $(this).next().fadeTo(400, 1, function () {
              $('.b-about__link').fadeTo(400, 1);
            });
          });
        });
      });
    }, 250);
  }

  $('.js-doctors-slider').owlCarousel({
    items: 4,
    loop: true,
    stagePadding: 30,
    margin: 80,
    nav: false,
    autoplay: true,
    mouseDrag: false,
    smartSpeed: 300,
    dotsSpeed: 0,
    autoplayTimeout: 5000,
    autoplayHoverPause: false,
    responsive: {
      0: {
        stagePadding: 10,
        dotsSpeed: 300,
        margin: 11,
        dots: true,
        items: 1
      },
      420: {
        stagePadding: 10,
        dotsSpeed: 300,
        margin: 11,
        dots: true,
        items: 2
      },
      481: {
        stagePadding: 10,
        dotsSpeed: 300,
        margin: 11,
        dots: true,
        items: 2
      },
      641: {
        stagePadding: 20,
        dotsSpeed: 300,
        margin: 21,
        dots: true,
        items: 3
      },
      769: {
        stagePadding: 20,
        dotsSpeed: 300,
        margin: 21,
        dots: true,
        items: 3
      },
      1025: {
        margin: 70,
        items: 4
      },
      1280: {
        items: 4
      }
    }
  });
  $('.js-doctors-slider .owl-dot').on('click', function () {
    if ($(window).width() > 1024) {
      $('.js-doctors-slider .owl-stage-outer').addClass('tr');
    }
  });
  $('.js-doctors-slider').on('changed.owl.carousel', function () {
    setTimeout(function () {
      $('.js-doctors-slider .owl-stage-outer').addClass('vs');
      setTimeout(function () {
        $('.js-doctors-slider .owl-stage-outer').removeClass('tr').removeClass('vs');
      }, 200);
    }, 200);
  });
  $('.js-sertificates-slider').owlCarousel({
    items: 4,
    loop: true,
    margin: 30,
    nav: true,
    autoplay: true,
    autoplayTimeout: 3500,
    autoplayHoverPause: false,
    smartSpeed: 250,
    navText: ['', ''],
    responsive: {
      0: {
        margin: 20,
        items: 1
      },
      420: {
        margin: 20,
        items: 2
      },
      // 480:{
      //     margin: 20,
      //     items: 2
      // },
      640: {
        items: 3
      },
      960: {
        items: 4
      }
    }
  });
  $('.js-sertificates-slider .owl-prev').append('<svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.49 22.14"><path class="cls-1" d="M10.28,26.48a1.12,1.12,0,0,0,1.59-1.57l-8-8H30.38a1.11,1.11,0,0,0,1.11-1.11,1.12,1.12,0,0,0-1.11-1.13H3.82l8-8a1.14,1.14,0,0,0,0-1.59,1.11,1.11,0,0,0-1.59,0L.33,15a1.09,1.09,0,0,0,0,1.57Z" transform="translate(0 -4.67)"/></svg>');
  $('.js-sertificates-slider .owl-next').append('<svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.49 22.14"><path class="cls-1" d="M21.2,5a1.12,1.12,0,1,0-1.59,1.57l8,8H1.11A1.11,1.11,0,0,0,0,15.74a1.12,1.12,0,0,0,1.11,1.13H27.67l-8,8a1.14,1.14,0,0,0,0,1.59,1.11,1.11,0,0,0,1.59,0l10-10a1.09,1.09,0,0,0,0-1.57Z" transform="translate(0 -4.67)"/></svg>');

  (function () {
    var $accordion = $('.c-accordion');
    var $accordionItem = $('.c-accordion__item');
    var $accordionBody = $('.c-accordion__item-in');
    var $activeAccordionItem = $('.c-accordion__item.is-active');

    if (!$accordion.length) {
      return;
    }

    $accordionItem.on('click', function (event) {
      event.preventDefault();
      var $this = $(this);

      if ($('.c-accordion__item-desc').length) {
        if ($this.hasClass('is-active')) {
          $this.closest($accordion).find($accordionItem).removeClass('is-active').find('.c-accordion__item-desc').slideUp();
          return;
        }

        $this.closest($accordion).find($accordionItem).removeClass('is-active').find('.c-accordion__item-desc').slideUp();
        $this.find('.c-accordion__item-desc').slideDown();
        $this.addClass('is-active');

        if ($(window).width() <= 768) {
          setTimeout(function () {
            $('html, body').animate({
              scrollTop: $this.offset().top - 55
            }, 300);
          }, 500);
        }
      } else {
        if ($this.hasClass('is-active')) {
          $this.closest($accordion).find($accordionItem).removeClass('is-active');
          return;
        }

        $this.closest($accordion).find($accordionItem).removeClass('is-active');
        $this.addClass('is-active');

        if ($(window).width() <= 768) {
          setTimeout(function () {
            $('html, body').animate({
              scrollTop: $this.offset().top - 80
            }, 300);
          }, 500);
        }
      }
    });
  })();

  ;

  (function () {
    var $titles = $('._js-tab-title');
    var $bodys = $('._js-tab-content');
    var $container = $('._js-tab');

    if (!$titles.length) {
      return;
    }

    $titles.on('click', function (event) {
      event.preventDefault();
      var $this = $(this);
      var indx = $this.data('tab-index');

      if ($this.hasClass('is-active')) {
        return;
      }

      $this.closest($container).find($titles).removeClass('is-active');
      $this.addClass('is-active');
      $this.closest($container).find($bodys).removeClass('is-active').hide().filter('[data-tab-index="' + indx + '"]').fadeIn();
      chahgeFeedbackIconRefresh();
      chahgeFeedbackIcon();
    });

    function chahgeFeedbackIconRound() {
      if ($('.c-feedback__icon.is-active').next('.c-feedback__icon').length) {
        $('.c-feedback').find('.c-feedback__icon.is-active').removeClass('is-active').next().addClass('is-active');
      } else {
        $('.c-feedback__icon').removeClass('is-active');
        $('.c-feedback__icon').first().addClass('is-active');
      }

      var activeIndex = $('.c-feedback__icon.is-active').data('tab-index');
      $('.c-feedback').find('.c-feedback__icon-desc').removeClass('is-active').hide().filter('[data-tab-index="' + activeIndex + '"]').fadeIn();
    }

    var refreshFn;

    function chahgeFeedbackIcon() {
      refreshFn = setInterval(function () {
        chahgeFeedbackIconRound();
      }, 4000);
    }

    function chahgeFeedbackIconRefresh() {
      clearInterval(refreshFn);
    }

    chahgeFeedbackIcon();
  })(); // chahgeFeedbackIcon();


  function cFeedbackIconsHeight() {
    setTimeout(function () {
      $('.c-feedback__icons').css({
        'height': $('.c-feedback__icons').innerWidth()
      });
    }, 250);
  }

  $.fn.toggleAttr = function (attr, attr1, attr2) {
    return this.each(function () {
      var self = $(this);
      if (self.attr(attr) == attr1) self.attr(attr, attr2);else self.attr(attr, attr1);
    });
  };

  $('.b-form__user-photo input[type="file"]').styler({
    filePlaceholder: '<span>Добавить <br> свою фотографию</span>'
  });
  $('select').styler();
  $('.dtoggle-display').on('click', function () {
    $(this).find('.line').toggleClass('hide');
    $(this).parent().find('input').toggleAttr('type', 'password', 'text');
  });
  $('._js-c-slider').each(function () {
    $(this).owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      margin: 0,
      nav: false,
      dots: true,
      navText: ['', '']
    });
  });
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  $('input[type=tel]').mask("+7 (999) 999 - 99 - 99");
  $(window).on('resize load', function () {
    headerResize();
    carouselResize();
    cFeedbackIconsHeight();
  });
  var bmPst;
  var whSt = 0;
  $(document).on('scroll load', function () {
    // bmPst = $('.index-1').offset().top - $(window).height()
    whSt = $(document).scrollTop();

    if ($('.b-about__items').length) {
      if ($(document).scrollTop() >= $('.b-about__items').offset().top - 321) {
        showIcons();
      }
    }

    $('.op-bl').each(function () {
      if (whSt >= $(this).offset().top - $(window).height() + 100) {
        $(this).addClass('show');
      }
    });
  });
  $('.op-bl').each(function () {
    if ($(document).scrollTop() >= $(this).offset().top - $(window).height()) {
      $(this).addClass('show');
    }
  });
  var galleryArrow = '<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path id="rght_copy" data-name="rght copy" class="cls-1" d="M680,2474a20,20,0,1,1,20-20A20.021,20.021,0,0,1,680,2474Zm0-38a18,18,0,1,0,18,18A18.018,18.018,0,0,0,680,2436Zm3.777,24.33a1.014,1.014,0,0,1-1.42,0,0.994,0.994,0,0,1,0-1.41l3.915-3.92H671.313a0.994,0.994,0,0,1-1-.99,1,1,0,0,1,1-1.01h14.968l-3.916-3.91a1,1,0,0,1,1.412-1.41l5.619,5.62a0.992,0.992,0,0,1,0,1.41Z"transform="translate(-660 -2434)"/></svg>';
  $('.lightGallery-container').lightGallery({
    thumbnail: false,
    autoplayControls: false,
    fullScreen: false,
    download: false,
    counter: false,
    toogleThumb: false,
    thumbMargin: 20,
    thumbWidth: 150,
    thumbContHeight: 115,
    loop: false,
    hash: false,
    zoom: 1,
    actualSize: false,
    selector: '.lightGallery-item',
    prevHtml: galleryArrow,
    nextHtml: galleryArrow
  });
});