/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = Object.assign({
    tag: 'div',
    type: 'icons',
    class: '',
    mode: 'inline',
    url: '',
  }, opts);

  let external = '';
  let typeClass = '';

  if (opts.mode === 'external') {
    external = `${opts.url}/sprite.${opts.type}.svg`;
  }

  if (opts.type !== 'icons') {
    typeClass = ` svg-icon--${opts.type}`;
  }

  opts.class = opts.class ? ` ${opts.class}` : '';

  return `
    <${opts.tag} class="svg-icon svg-icon--${name}${typeClass}${opts.class}" aria-hidden="true" focusable="false">
      <svg class="svg-icon__link">
        <use xlink:href="${external}#${name}"></use>
      </svg>
    </${opts.tag}>
  `;
}

/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function() {

  'use strict';

  /**
   * определение существования элемента на странице
   */
  $.exists = (selector) => $(selector).length > 0;

  //=require ../_blocks/**/*.js

  $('input[type=tel]').mask("+7 (999) 999 - 99 - 99");

  $(window).on('resize load', function(){
    headerResize();
    carouselResize();
    cFeedbackIconsHeight();
  });

  var bmPst;
  var whSt = 0;

  $(document).on('scroll load', function(){
    // bmPst = $('.index-1').offset().top - $(window).height()
    whSt = $(document).scrollTop()

    if($('.b-about__items').length){
      if($(document).scrollTop() >= $('.b-about__items').offset().top - 321) {
        showIcons();
      }
    }

    $('.op-bl').each(function(){
      if(whSt >= ($(this).offset().top - $(window).height() + 100)) {
        $(this).addClass('show') 
      }
    });


  });

  $('.op-bl').each(function(){
    if($(document).scrollTop() >= ($(this).offset().top - $(window).height())) {
      $(this).addClass('show') 
    }
  });

  var galleryArrow = '<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path id="rght_copy" data-name="rght copy" class="cls-1" d="M680,2474a20,20,0,1,1,20-20A20.021,20.021,0,0,1,680,2474Zm0-38a18,18,0,1,0,18,18A18.018,18.018,0,0,0,680,2436Zm3.777,24.33a1.014,1.014,0,0,1-1.42,0,0.994,0.994,0,0,1,0-1.41l3.915-3.92H671.313a0.994,0.994,0,0,1-1-.99,1,1,0,0,1,1-1.01h14.968l-3.916-3.91a1,1,0,0,1,1.412-1.41l5.619,5.62a0.992,0.992,0,0,1,0,1.41Z"transform="translate(-660 -2434)"/></svg>';
  $('.lightGallery-container').lightGallery({
    thumbnail: false,
    autoplayControls: false,
    fullScreen: false,
    download: false,
    counter: false,
    toogleThumb: false,
    thumbMargin: 20,
    thumbWidth: 150,
    thumbContHeight: 115,
    loop: false,
    hash: false,
    zoom: 1,
    actualSize: false,
    selector: '.lightGallery-item',
    prevHtml: galleryArrow,
    nextHtml: galleryArrow
  });

});
