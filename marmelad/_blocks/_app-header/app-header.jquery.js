
function documentAddClassOverflowHidden() {
	if (/iPod|iPad|iPhone/i.test(navigator.userAgent)) {
		$('body').addClass('overflow-hidden');
	} else {
		$('body').removeClass('overflow-hidden');
		$(document.documentElement).addClass('overflow-hidden');
	}
}

function documentRemoveClassOverflowHidden() {
	$('body').removeClass('overflow-hidden');
	$(document.documentElement).removeClass('overflow-hidden');
}

var headerResizeFlag = 1;

function headerResize() {
	if (window.matchMedia('(max-width:1024px)').matches && headerResizeFlag == 1) {
		if($('.app-header--type-2').length) {
			$('.app-header__content-bot .app-header__container').appendTo('.mobile-panel__content')
			$('.app-header__search').appendTo('.mobile-panel__content')
		} else {
			$('.app-header__content-bot').appendTo('.mobile-panel__content')
		}
		$('.app-header__menu').appendTo('.mobile-panel__content')
		headerResizeFlag = 2;
	} else if (window.matchMedia('(min-width:1025px)').matches && headerResizeFlag == 2) {
		if($('.app-header--type-2').length){
			$('.mobile-panel__content .app-header__container').appendTo('.app-header__content-bot')
			$('.app-header__menu').prependTo('.app-header__content-top .app-header__container')
			$('.app-header__search').appendTo('.app-header__content-top .app-header__container-bot')
		} else {
			$('.app-header__menu').appendTo('.app-header__content-top .app-header__container')
			$('.app-header__content-bot').appendTo('.app-header__content')
		}
		headerResizeFlag = 1;
	}
}

$('.app-header__burger').on('click tap', function(){
	$('.mobile-panel').addClass('opened')
	$('.app__overlay').addClass('opened')
	documentAddClassOverflowHidden();
	$('.mobile-panel').css({
    	'height': $(window).height() + 60
	})
});

$(document).on('click tap', function(event){
  if ($(event.target).closest('.app-header__burger, .mobile-panel__content').length) return;
   	    	
  $('.mobile-panel').removeClass('opened')
  $('.app__overlay').removeClass('opened')
	documentRemoveClassOverflowHidden();
});


var bCarousel = $('.b-carousel__items'),
	items = $('.b-carousel__item'),
	activeItem = bCarousel.find('.active-item'),
	rotateDeg = 90;

// bCarousel.css({'transform' : 'rotate('+ 90 +'deg)'});

function carouselResize() {
	if($(window).width() > 1800) {
		$('.b-carousel__item.active-item').css({
			"transform": "rotate(-90deg) scale(1.44)"	
		})
		carouselRefreshInterval();
		carouselRotateInterval();
	} else {
		carouselRefreshInterval();
		$('.b-carousel__item').removeAttr('style')
		$('.b-carousel__items').removeAttr('style')
	}
}

function rotateCarouselBack() {
	rotateDeg -= 45
	items.css({'transform' : 'rotate('+ -rotateDeg +'deg)'});	
	bCarousel.css({'transform' : 'rotate('+ rotateDeg +'deg)'});

	setTimeout(function(){
		if($('.b-carousel__item.active-item').next().length == 0) {
			items
				.removeClass('active-item')	
				.css({'transform' : 'rotate('+ -rotateDeg+ 'deg)'})
				.first()
				.addClass('active-item')
				.css({'transform' : 'rotate('+ -rotateDeg+ 'deg) scale(1.44)'})
		}
		else {
			$('.b-carousel__item.active-item')
				.next()
				.addClass('active-item')
				.css({'transform' : 'rotate('+ -rotateDeg+ 'deg) scale(1.44)'})
				.prev()
				.removeClass('active-item')
		}
		changeBlock();
	}, 100)		
}

function rotateCarouselForward() {
	rotateDeg += 45
	items.css({'transform': 'rotate('+ -rotateDeg +'deg)'});	
	bCarousel.css({'transform': 'rotate('+ rotateDeg +'deg)'});	

	setTimeout(function(){
		if($('.b-carousel__item.active-item').prev().length == 0) {
			items
				.removeClass('active-item')	
				.css({'transform' : 'rotate('+ -rotateDeg+ 'deg)'})
				.last()
				.addClass('active-item')
				.css({'transform' : 'rotate('+ -rotateDeg+ 'deg) scale(1.44)'})						
		} else {
			$('.b-carousel__item.active-item')
				.prev()
				.addClass('active-item')
				.css({'transform' : 'rotate('+ -rotateDeg+ 'deg) scale(1.44)'})
				.next()
				.removeClass('active-item')
		}
		changeBlock();
	}, 100)	
}

$('.b-carousel__arrow').on('click', function(){
	var $this = $(this);
	carouselRefreshInterval();
	carouselRotateInterval();
	if($this.hasClass('prev')) {
		rotateCarouselBack();
	} else {
		rotateCarouselForward();
	}
});


var refresh;

function carouselRotateInterval(){
    refresh = setInterval(function(){ rotateCarouselForward() }, 5000);
}

function carouselRefreshInterval(){
    clearInterval(refresh);
}

function changeBlock() {
	var dataTabIndex = $('.b-carousel').find('.b-carousel__item.active-item').data('tab-index');
	$('.b-info .b-info__content')
		.hide()
		.removeClass('active-item')
		.filter('[data-tab-index='+ dataTabIndex +']')
		.fadeIn()
		.addClass('active-item')
		.css({'display': $(window).width() > 768 ? 'flex' : 'block'})
}

$('.b-carousel__item').on('click', function(){
	if($(window).width() < 1800) {
		$('.b-carousel__item').removeClass('active-item')
		$(this).addClass('active-item')
		changeBlock();
	}
})

if($(window).width() > 1800) {
	$('.b-info').on({
		mouseenter: function() {
	    	carouselRefreshInterval();
	  	}, mouseleave: function() {
	    	carouselRotateInterval()
	  	}
	})
}

$('.has-sublevel > a').append('<span class="arrow toggle-menu"></span>')

$(document).on('click', '.toggle-menu', function(event){
	event.preventDefault();
	$(this).closest('li').toggleClass('opened')
})

var feedbackPopup = $('[data-remodal-id=feddbackModal]').remodal();
$('._js-feedback').on('click', function (event) {
	event.preventDefault();
	feedbackPopup.open();
});

var authorization = $('[data-remodal-id=authorizationModal]').remodal();
$('._js-authorization').on('click', function (event) {
	event.preventDefault();
	authorization.open();
});

$('.remodal-close').append('<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.97 13.97"><title>close</title><path d="M19,6.42,13.41,12,19,17.58,17.58,19,12,13.41,6.42,19,5,17.58,10.59,12,5,6.42,6.42,5,12,10.59,17.58,5Z" transform="translate(-5.02 -5.02)"/></svg>')