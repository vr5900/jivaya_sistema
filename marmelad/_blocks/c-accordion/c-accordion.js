;(function() {
    var $accordion = $('.c-accordion');
    var $accordionItem = $('.c-accordion__item');
    var $accordionBody = $('.c-accordion__item-in');
    var $activeAccordionItem = $('.c-accordion__item.is-active');

    if (!$accordion.length) {
        return;
    }

    $accordionItem.on('click', function (event) {      
        event.preventDefault();
        var $this = $(this);


        if($('.c-accordion__item-desc').length) {
            if ($this.hasClass('is-active')) {
              $this
                .closest($accordion)
                .find($accordionItem)
                .removeClass('is-active') 
                .find('.c-accordion__item-desc') 
                .slideUp()
              return;
            }

            $this
                .closest($accordion)
                .find($accordionItem)
                .removeClass('is-active') 
                .find('.c-accordion__item-desc') 
                .slideUp()

            $this
                .find('.c-accordion__item-desc')
                .slideDown()
            $this
                .addClass('is-active')

            if($(window).width() <= 768) {
                setTimeout(function(){
                    $('html, body').animate({scrollTop: $this.offset().top - 55}, 300)    
                }, 500)
            }    
        } else {
            if ($this.hasClass('is-active')) {
              $this
                .closest($accordion)
                .find($accordionItem)
                .removeClass('is-active') 
              return;
            }
            $this.closest($accordion).find($accordionItem).removeClass('is-active')
            $this.addClass('is-active')
            if($(window).width() <= 768) {
                setTimeout(function(){
                    $('html, body').animate({scrollTop: $this.offset().top - 80}, 300)    
                }, 500)
            }

        }


    });

})();