$('.js-sertificates-slider').owlCarousel({
    items: 4,
    loop: true,
    margin: 30,
    nav: true,
    autoplay: true,
    autoplayTimeout: 3500,
    autoplayHoverPause: false,
    smartSpeed: 250, 
    navText: ['',''],
    responsive:{
        0:{
            margin: 20,
            items: 1
        },
        420:{
            margin: 20,
            items: 2
        },
        // 480:{
        //     margin: 20,
        //     items: 2
        // },
        640:{
            items: 3
        },
        960:{
            items: 4
        }
    }
});

$('.js-sertificates-slider .owl-prev').append('<svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.49 22.14"><path class="cls-1" d="M10.28,26.48a1.12,1.12,0,0,0,1.59-1.57l-8-8H30.38a1.11,1.11,0,0,0,1.11-1.11,1.12,1.12,0,0,0-1.11-1.13H3.82l8-8a1.14,1.14,0,0,0,0-1.59,1.11,1.11,0,0,0-1.59,0L.33,15a1.09,1.09,0,0,0,0,1.57Z" transform="translate(0 -4.67)"/></svg>')
$('.js-sertificates-slider .owl-next').append('<svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.49 22.14"><path class="cls-1" d="M21.2,5a1.12,1.12,0,1,0-1.59,1.57l8,8H1.11A1.11,1.11,0,0,0,0,15.74a1.12,1.12,0,0,0,1.11,1.13H27.67l-8,8a1.14,1.14,0,0,0,0,1.59,1.11,1.11,0,0,0,1.59,0l10-10a1.09,1.09,0,0,0,0-1.57Z" transform="translate(0 -4.67)"/></svg>')