$.fn.toggleAttr = function(attr, attr1, attr2) {
  return this.each(function() {
    var self = $(this);
    if (self.attr(attr) == attr1)
      self.attr(attr, attr2);
    else
      self.attr(attr, attr1);
  });
};


$('.b-form__user-photo input[type="file"]').styler({
	filePlaceholder: '<span>Добавить <br> свою фотографию</span>'
})
$('select').styler()

$('.dtoggle-display').on('click', function(){
	$(this).find('.line').toggleClass('hide')
	$(this).parent().find('input').toggleAttr('type', 'password', 'text');
})