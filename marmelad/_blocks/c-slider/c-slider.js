$('._js-c-slider').each(function(){
	$(this).owlCarousel({
	    items: 1,
	    loop: true,
	    autoplay: true,
	    margin: 0,
	    nav: false,
	    dots: true,
	    navText: ['','']
	})
})
