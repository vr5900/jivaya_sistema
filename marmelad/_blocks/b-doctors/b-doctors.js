$('.js-doctors-slider').owlCarousel({
    items: 4,
    loop: true,
    stagePadding: 30,
    margin: 80,
    nav: false,
    autoplay: true,
    mouseDrag: false,
    smartSpeed: 300,
    dotsSpeed: 0,
    autoplayTimeout: 5000,
    autoplayHoverPause: false,
    responsive:{
        0:{
            stagePadding: 10,
            dotsSpeed: 300,
            margin: 11, 
            dots: true,
            items: 1
        },
        420:{
            stagePadding: 10,
            dotsSpeed: 300,
            margin: 11, 
            dots: true,
            items: 2
        },
        481:{
            stagePadding: 10,
            dotsSpeed: 300,
            margin: 11, 
            dots: true,
            items: 2
        },
        641:{
            stagePadding: 20,
            dotsSpeed: 300,
            margin: 21, 
            dots: true,
            items: 3
        },
        769:{
            stagePadding: 20,
            dotsSpeed: 300,
            margin: 21, 
            dots: true,
            items: 3
        },
        1025:{
            margin: 70,
            items: 4
        },
        1280:{
            items: 4
        }
    }
});

$('.js-doctors-slider .owl-dot').on('click', function(){
    if($(window).width() > 1024 ) {
        $('.js-doctors-slider .owl-stage-outer').addClass('tr')     
    }
})

$('.js-doctors-slider').on('changed.owl.carousel', function(){
    setTimeout(function(){
        $('.js-doctors-slider .owl-stage-outer').addClass('vs')     
        setTimeout(function(){
            $('.js-doctors-slider .owl-stage-outer').removeClass('tr').removeClass('vs')     
        }, 200)
    }, 200)
})