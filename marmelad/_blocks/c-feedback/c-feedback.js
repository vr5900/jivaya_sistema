;(function () {
  var $titles = $('._js-tab-title');
  var $bodys = $('._js-tab-content');
  var $container = $('._js-tab');

  if (!$titles.length) {
    return;
  }

  $titles.on('click', function (event) {
    event.preventDefault();
    var $this = $(this);
    var indx = $this.data('tab-index');
    
    if ($this.hasClass('is-active')) {
      return;
    }


    $this.closest($container).find($titles).removeClass('is-active')

    $this.addClass('is-active');

    $this
      .closest($container)
      .find($bodys)
      .removeClass('is-active')
      .hide()
      .filter('[data-tab-index="' + indx + '"]')
      .fadeIn();

    chahgeFeedbackIconRefresh();  
    chahgeFeedbackIcon();

  });


  function chahgeFeedbackIconRound() {
    if($('.c-feedback__icon.is-active').next('.c-feedback__icon').length){
      $('.c-feedback').find('.c-feedback__icon.is-active').removeClass('is-active').next().addClass('is-active')
    } else {
      $('.c-feedback__icon').removeClass('is-active')
      $('.c-feedback__icon').first().addClass('is-active')
    }

    var activeIndex = $('.c-feedback__icon.is-active').data('tab-index');

    $('.c-feedback')
      .find('.c-feedback__icon-desc')
      .removeClass('is-active')
      .hide()
      .filter('[data-tab-index="' + activeIndex + '"]')
      .fadeIn();  


  }

  var refreshFn;

  function chahgeFeedbackIcon() {
    refreshFn = setInterval(function(){ chahgeFeedbackIconRound() }, 4000);
  }

  function chahgeFeedbackIconRefresh(){
    clearInterval(refreshFn);
  }

  chahgeFeedbackIcon();

})();


// chahgeFeedbackIcon();

function cFeedbackIconsHeight() {
  setTimeout(function(){
    $('.c-feedback__icons').css({'height': $('.c-feedback__icons').innerWidth()})
  }, 250)
}